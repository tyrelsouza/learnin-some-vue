PLAYER = 0;
MONSTER = 1;

var app = new Vue({
  el: '#app',
  data: {
    pHP: 100,
    mHP: 100,
    gameOn: false,
    logs: [],
  },
  watch: {
    pHP(value){
      if (value <= 0){
        alert("You lose");
        this.giveUp();
      }
    },
    mHP(value){
      if (value <= 0){
        alert("You Win!");
        this.giveUp();
      }
    }
  },
  methods:{
    start(){
      this.gameOn = true;
      this.mHP = 100;
      this.pHP = 100;
      this.logs = [];
    },
    playerAttack(){
      var dmg = this.getRandomInt(3, 10);
      this.mHP -= dmg;
      this.logs.unshift({isPlayer: true, text: 'Player hits monster for ' + dmg});
      this.monsterAttack();
    },
    specialAttack(){
      var dmg = this.getRandomInt(10, 20);
      this.mHP -= dmg;
      this.logs.unshift({isPlayer: true, text: 'Player hits monster hard for ' + dmg});
      this.monsterAttack();
    },
    heal(){
      var hp = 10
      this.pHP = Math.min(100, this.pHP + 10 );
      var log =
      this.logs.unshift({isPlayer: true, text: 'Player heals for 10'});
      this.monsterAttack();
    },
    giveUp(){
      this.gameOn = false;
      this.logs = [];
    },
    monsterAttack(){
      var dmg = this.getRandomInt(5, 12);
      var log = { isPlayer: false, text: 'monster hits player for ' + dmg}
      this.pHP -= dmg;
      this.logs.unshift(log);
    },
    getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  },
  computed: {

    mHPStyle(){
      return {width: this.mHP + "%"};
    },
    pHPStyle(){
      return {width: this.pHP + "%"};
    },
  }
})
